﻿namespace CompanyInfoApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCustomProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomProperties", "IsEnable", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomProperties", "IsEnable");
        }
    }
}
