﻿namespace CompanyInfoApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        CompanyId = c.Long(nullable: false, identity: true),
                        CompanyName = c.String(nullable: false),
                        Address = c.String(),
                        Website = c.String(),
                        EmailId = c.String(),
                    })
                .PrimaryKey(t => t.CompanyId);
            
            CreateTable(
                "dbo.CustomProperties",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CompanyId = c.Long(nullable: false),
                        CustomPro = c.String(),
                        Type = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .Index(t => t.CompanyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustomProperties", "CompanyId", "dbo.Companies");
            DropIndex("dbo.CustomProperties", new[] { "CompanyId" });
            DropTable("dbo.CustomProperties");
            DropTable("dbo.Companies");
        }
    }
}
