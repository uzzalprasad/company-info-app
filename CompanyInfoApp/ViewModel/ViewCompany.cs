﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CompanyInfoApp.ViewModel
{
    public class ViewCompany
    {
        public Int64 CompanyId { get; set; }
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }
        [DisplayName("Email Id")]
        public string EmailId { get; set; }
        [DisplayName("Custom Property Name")]
        public string CustomPro { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public bool IsEnable { get; set; }
    }
}