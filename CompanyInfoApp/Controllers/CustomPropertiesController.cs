﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CompanyInfoApp.Models;

namespace CompanyInfoApp.Controllers
{
    public class CustomPropertiesController : Controller
    {
        private DatabaseDbContext db = new DatabaseDbContext();

        // GET: CustomProperties/Create
        public ActionResult Create(long? CompanyId)
        {
            if (CompanyId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.CustomPropertyList = db.CustomPropertyDbSet.Where(c => c.CompanyId == CompanyId).ToList();
            CustomProperty customProperty = new CustomProperty();
            customProperty.CompanyId = (long)CompanyId;
            return View(customProperty);
        }

        // POST: CustomProperties/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CompanyId,CustomPro,Type,Value,IsEnable")] CustomProperty customProperty)
        {
            if (ModelState.IsValid)
            {
                db.CustomPropertyDbSet.Add(customProperty);
                db.SaveChanges();

                ViewBag.CustomPropertyList = db.CustomPropertyDbSet.Where(c => c.CompanyId == customProperty.CompanyId).ToList();
                return RedirectToAction("Create", new { CompanyId = customProperty.CompanyId });
            }

            ViewBag.CustomPropertyList = db.CustomPropertyDbSet.Where(c => c.CompanyId == customProperty.CompanyId).ToList();
            return View(customProperty);
        }

        // GET: CustomProperties1/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomProperty customProperty = db.CustomPropertyDbSet.Find(id);
            if (customProperty == null)
            {
                return HttpNotFound();
            }

            return View(customProperty);
        }

        // POST: CustomProperties1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CompanyId,CustomPro,Type,Value,IsEnable")] CustomProperty customProperty)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customProperty).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Create", new { CompanyId = customProperty.CompanyId });
            }

            return View(customProperty);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
