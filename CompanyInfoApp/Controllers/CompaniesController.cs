﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CompanyInfoApp.Models;
using CompanyInfoApp.ViewModel;

namespace CompanyInfoApp.Controllers
{
    public class CompaniesController : Controller
    {
        private DatabaseDbContext db = new DatabaseDbContext();

        // GET: Companies
        public ActionResult Index()
        {
            return View(db.CompanyDbSet.ToList());
        }

        // GET: Companies/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var companyInfo = (from m in db.CompanyDbSet
                               join n in db.CustomPropertyDbSet on m.CompanyId equals n.CompanyId
                               where m.CompanyId == id
                               select new ViewCompany()
                               {
                                   CompanyId = m.CompanyId,
                                   CompanyName = m.CompanyName,
                                   Address = m.Address,
                                   EmailId = m.EmailId,
                                   Website = m.Website,
                                   CustomPro = n.CustomPro,
                                   Type = n.Type,
                                   Value = n.Value,
                                   IsEnable = n.IsEnable
                               }).ToList();

            if (companyInfo == null)
            {
                return HttpNotFound();
            }
            ViewBag.companyInfo = companyInfo;
            return View(companyInfo.FirstOrDefault());
        }

        // GET: Companies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Companies/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CompanyId,CompanyName,Address,Website,EmailId")] Company company)
        {
            if (ModelState.IsValid)
            {
                db.CompanyDbSet.Add(company);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(company);
        }

        // GET: Companies/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = db.CompanyDbSet.Find(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CompanyId,CompanyName,Address,Website,EmailId")] Company company)
        {
            if (ModelState.IsValid)
            {
                db.Entry(company).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(company);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
