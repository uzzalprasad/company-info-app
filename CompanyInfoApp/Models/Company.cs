﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CompanyInfoApp.Models
{
    public class Company
    {
        [Key]
        public Int64 CompanyId { get; set; }
        [Required(ErrorMessage = "Company Name Required")]
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Invalid Email Address")]
        [DisplayName("Email Id")]
        public string EmailId { get; set; }
    }
}