﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CompanyInfoApp.Models
{
    public class CustomProperty
    {
        [Key]
        public Int64 Id { get; set; }
        [Required(ErrorMessage = "Company Name Required")]
        public Int64 CompanyId { get; set; }
        public virtual Company Company { get; set; }
        [DisplayName("Custom Property Name")]
        public string CustomPro { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public bool IsEnable { get; set; }
    }
}