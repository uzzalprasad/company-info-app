﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace CompanyInfoApp.Models
{
    public class DatabaseDbContext : DbContext
    {
        public DatabaseDbContext() : base("Data Source =(local); Initial Catalog = CompanyDb; Integrated Security = True;") { }
        public DbSet<Company> CompanyDbSet { get; set; }
        public DbSet<CustomProperty> CustomPropertyDbSet { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

        }
    }
}